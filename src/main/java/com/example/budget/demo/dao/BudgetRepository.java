package com.example.budget.demo.dao;

import com.example.budget.demo.entities.Budget;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BudgetRepository extends JpaRepository<Budget,Long> {

}
