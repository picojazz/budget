package com.example.budget.demo.dao;


import com.example.budget.demo.entities.Budget;
import com.example.budget.demo.entities.Role;
import com.example.budget.demo.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl  implements UserService{
    @Autowired
    private UserRepository ur;
    @Autowired
    private RoleRepository rr;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private BudgetRepository br;



    @Override
    public void saveUser(User user) {

            Role role = rr.findByRole("user");
            List<Role> roles = new ArrayList<>();
            roles.add(role);
            user.setRoles(roles);
            user.setActive(1);
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
           User newUser= ur.save(user);

        br.save(new Budget(0,newUser));




    }
}
