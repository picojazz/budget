package com.example.budget.demo.dao;

import com.example.budget.demo.entities.User;


public interface UserService {

    public void saveUser(User user);
}
