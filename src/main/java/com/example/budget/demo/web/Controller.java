package com.example.budget.demo.web;


import com.example.budget.demo.dao.BudgetRepository;
import com.example.budget.demo.dao.TransactionRepository;
import com.example.budget.demo.dao.UserRepository;
import com.example.budget.demo.dao.UserService;
import com.example.budget.demo.entities.Budget;
import com.example.budget.demo.entities.Transaction;
import com.example.budget.demo.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@org.springframework.stereotype.Controller
public class Controller {
    @Autowired
    private UserService us;
    @Autowired
    private UserRepository ur;
    @Autowired
    private BudgetRepository br;
    @Autowired
    private TransactionRepository tr;

    @RequestMapping(value = "/login")
    public String login(Model model){
        model.addAttribute("user",new User());
        return "login";
    }

    @RequestMapping(value = "/logout")
    public String logout(HttpServletRequest req, HttpServletResponse resp){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(req,resp,auth);
        }
        return "redirect:login?logout";
    }
    @RequestMapping(value = "/signup")
    public String addUser(@Valid User user, BindingResult bindingResult, RedirectAttributes redirectAttributes){

        if(ur.findByEmail(user.getEmail()) != null){
            bindingResult.rejectValue("email","error.user","Cet email est deja utilisé");
        }
        if(ur.findByUsername(user.getUsername()) != null){
            bindingResult.rejectValue("username","error.user","Ce username est deja utilisé");
        }

        if(bindingResult.hasErrors()){
            return "login";
        }

        us.saveUser(user);
        redirectAttributes.addFlashAttribute("type", "alert alert-success");
        redirectAttributes.addFlashAttribute("message", "Inscription reussie . veuillez vous connecter maintenant !");

        return "redirect:login";
    }

    @RequestMapping(value = "/")
    public String me(Model model, Principal principal){
        User user =  ur.findByUsername(principal.getName());
        List<Budget> ls = user.getBudgets();
        Budget b = ls.get(ls.size()-1);
        model.addAttribute("budget",b);
        model.addAttribute("montAct",b.getMontant());

        return "me";

    }
    @RequestMapping(value = "/new")
    public String meAdd(Model model, Principal principal,Double montant){
        User user =  ur.findByUsername(principal.getName());

        br.save(new Budget(montant,user));

        return "redirect:/";

    }
    @RequestMapping(value = "/add")
    public String Add(Model model,Long idBudget,Double montant,String type,String description){
        Budget b = br.findOne(idBudget);
        double m = b.getMontant();
        if(type.equals("retrait")){
            b.setMontant(m-montant);
        }else{
            b.setMontant(m+montant);
        }
        Transaction tran = new Transaction(montant,description,b,type);
        tr.save(tran);
        br.save(b);



        return "redirect:/";

    }

}
